#!/usr/bin/python
#
# Génération de index.html depuis index.yaml

# Imports
import yaml

ICONS = {
  'Retraite':     'retraite',
  'Majorité':     'majorite',
  'Vote':         'vote',
  'Travailleurs': 'travailleurs',
  'Femmes':       'femmes',
  'Esclavage':    'esclavage',
  'Luttes':       'luttes',
}
TITRES = {
  'Retraite':     'Retraite',
  'Majorité':     'Majorité',
  'Vote':         'Droit de vote',
  'Travailleurs': 'Droits du travail',
  'Femmes':       'Droits des femmes',
  'Esclavage':    'Esclavage',
  'Luttes':       'Luttes',
}

TIMELINE_BLOCK = """
		<div class="cd-timeline-block">
			<div class="cd-timeline-img ICON">
				<img src="img/icon-ICON.png" alt="CATEGORIE" title="CATEGORIE">
			</div> <!-- cd-timeline-img -->

			<div class="cd-timeline-content">
				<h2>TITRE</h2>TEXTELIEN
				<span class="cd-date">DATE</span>
			</div> <!-- cd-timeline-content -->
		</div> <!-- cd-timeline-block -->"""

with open('index.yaml', 'r') as stream:
    tl_data = yaml.load(stream)

timeline = ''
for date in sorted(tl_data):
    data = tl_data[date]
    print(date)
    print(tl_data[date])
    tl_item = TIMELINE_BLOCK
    tl_item = tl_item.replace('ICON', ICONS[data['Catégorie']])
    tl_item = tl_item.replace('CATEGORIE', TITRES[data['Catégorie']])
    tl_item = tl_item.replace('TITRE', data['Titre'])
    if 'Texte' in data:
        tl_item = tl_item.replace('TEXTE',
                                  '\n                               ' +
                                  '<p>'+data['Texte']+'</p>')
    else:
        tl_item = tl_item.replace('TEXTE', '')
    if 'Lien' in data:
        tl_item = tl_item.replace('LIEN',
                                  '\n                               ' +
                    '<a target="_blank" href="'+data['Lien']+'" class="cd-read-more">En savoir plus</a>')
    else:
        tl_item = tl_item.replace('LIEN', '')
    if 'Date' in data:
        date = data['Date']
    tl_item = tl_item.replace('DATE', str(date))
    timeline = timeline + tl_item

with open('index-template.html', 'r') as template_file:
    index = template_file.read()

index = index.replace('TIMELINE', timeline)

with open('index.html', 'w') as file:
    file.write(index)
